import csv

import os

import json

evaluations = json.load(open(os.path.join('..', 'assets', '2024-09-11_fair-enough_evaluations.json'), 'r', encoding="utf8"))

print(len(evaluations))

sum_data, sum_metadata = 0, 0
count_data, count_metadata = 0, 0


for entry in evaluations:
    if entry['collection'] == 'fair-enough-data':
        count_data += 1
        sum_data += entry['score_percent']

    if entry['collection'] == 'fair-enough-metadata':
        count_metadata += 1
        sum_metadata += entry['score_percent']


print('data', count_data, sum_data/count_data)
print('metadata', count_metadata, sum_metadata/count_metadata)


my_evaluations = csv.DictReader(open(os.path.join('..', 'assets', '2024-09-12_FAIR Enough.csv'), 'r', encoding="utf8"))

my_sum_data, my_sum_metadata = 0, 0
my_count_data, my_count_metadata = 0, 0

my_sum_profile_data, my_sum_profile_metadata = 0, 0
my_count_profile_data, my_count_profile_metadata = 0, 0

my_sum_metadata_data, my_sum_metadata_metadata = 0, 0
my_count_metadata_data, my_count_metadata_metadata = 0, 0

for entry in my_evaluations:
    if entry['Collection'] == 'fair-enough-data':
        my_count_data += 1
        my_sum_data += eval(entry['Completion'])
        if 'Profile' in entry['Resource URI']:
            my_count_profile_data += 1
            my_sum_profile_data += eval(entry['Completion'])
        else:
            my_count_metadata_data += 1
            my_sum_metadata_data += eval(entry['Completion'])

    if entry['Collection'] == 'fair-enough-metadata':
        my_count_metadata += 1
        my_sum_metadata += eval(entry['Completion'])
        if 'Profile' in entry['Resource URI']:
            my_count_profile_metadata += 1
            my_sum_profile_metadata += eval(entry['Completion'])
        else:
            my_count_metadata_metadata += 1
            my_sum_metadata_metadata += eval(entry['Completion'])


print('my data', my_count_data, my_sum_data/my_count_data)
print('my metadata', my_count_metadata, my_sum_metadata/my_count_metadata)

print('my profile data', my_count_profile_data, my_sum_profile_data/my_count_profile_data)
print('my profile metadata', my_count_profile_metadata, my_sum_profile_metadata/my_count_profile_metadata)

print('my metadata data', my_count_metadata_data, my_sum_metadata_data/my_count_metadata_data)
print('my metadata metadata', my_count_metadata_metadata, my_sum_metadata_metadata/my_count_metadata_metadata)