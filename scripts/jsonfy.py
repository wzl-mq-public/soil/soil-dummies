import json

import os

directory0 = '../out/fair-enough_fair-enough-data'
content0 = os.listdir(directory0)

for entry0 in content0:
    directory1 = os.path.join(f'{directory0}/{entry0}')
    content1 = os.listdir(directory1)

    for entry1 in content1:
        directory2 = os.path.join(f'{directory1}/{entry1}')
        content2 = os.listdir(directory2)

        for entry2 in content2:
            file = os.path.join(f'{directory2}/{entry2}')
            body = json.load(open(file, 'r'))

            if isinstance(body, str):
                body = json.loads(body)

            print(body)
            json.dump(body, open(file, 'w'))
            pass