import toml

import start
from hwc.device import Device
from com_ipp import COMIpp

if __name__ == '__main__':
    device = Device()
    soilClient = COMIpp(device)
    device.start()

    start.start(soilClient, toml.load('config.toml'), 'model.json')