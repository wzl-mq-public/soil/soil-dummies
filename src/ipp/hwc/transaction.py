from __future__ import annotations

import time
from abc import ABC, abstractmethod
import re
import typing
from sys import modules
import json
import os.path

import requests

soil_model = json.load(open(os.path.join(os.path.dirname(__file__), os.pardir, 'model.json')))

if typing.TYPE_CHECKING:
    from .device import Device, Tool


class TransactionNotImplementedException(Exception):
    def __init__(self):
        self.message = "Tried to use an unimplemented Transaction."


class Transaction(ABC):

    def __init__(self, tag: str, device: Device):
        self._tag = tag
        self._device = device

    @property
    def tag(self) -> str:
        return self._tag

    @abstractmethod
    def update_device(self, data):
        ...

    def on_complete(self):
        self._device.ipp_client.transaction_log.pop(self.tag)


class UnknownCommandTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class EnumPropTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)
        self.props = {}

    def update_device(self, data):
        data = data.split(',')
        for i in range(len(data)):
            data[i] = data[i].strip('\"')
        self.props[data[0]] = {'type': data[1], 'value': None}

    def on_complete(self):
        """ Signature is always of form "EnumProp(...()), therefore the first 9 and
            last 3 characters can be cut off to retrieve the actual signature of the prop.
            e.g. "EnumProp(Tool.ScanPar())" would deliver Tool.ScanPar
        """
        key = self._device.ipp_client.transaction_log[self.tag]['uuid'][9:-3]
        self._device.active_tool.build_prop(key, self.props)
        super().on_complete()


class GetPropTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        """Data is always of form "Tool.key_1.key_2. ... .key_n(data)"

        Therefore, it can always be split at the first occurrence of "(".
        Then the first part is split again at the dots to receive all keys and the
        last char of the second part is removed to receive all data without the
        unnecessary trailing ")".
        """
        data = data.split('(', 1)

        keys = data[0].split('.')
        value = data[1][:-1]
        self._device.active_tool.set_prop(keys, value)


class SetPropTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class OnMoveReportTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        print("OnMoveReport update: " + '[%s]' % ', '.join(map(str, data)))
        for data_point in data:
            axis = data_point.split("(")[0]
            value = re.findall(r"-?\d+\.\d+", data_point)
            value = value[0]
            match axis:
                case "X":
                    self._device._x = float(value)
                case "Y":
                    self._device._y = float(value)
                case "Z":
                    self._device._z = float(value)
        # self._tool.update([self._x, self._y, self._z])


class OnPtMeasReportTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        print("OnPtMeasReport update: " + '[%s]' % ', '.join(map(str, data)))
        # for data_point in data:
        #     axis = data_point.split("(")[0]
        #     value = re.findall(r"-?\d+\.\d+", data_point)
        #     value = value[0]
        #     match axis:
        #         case "X":
        #             self._device._x = float(value)
        #         case "Y":
        #             self._device._y = float(value)
        #         case "Z":
        #             self._device._z = float(value)
        # self._tool.update([self._x, self._y, self._z])


class StartSessionTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class GetSupportedCommandsTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class ChangeToolTransaction(Transaction):
    """Orders the Server to change the current Tool. Afterward home the active tool.
        TODO: Somehow fetch the home position of the tool as (0, 0, 0) might not be guaranteed.
    """

    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class IsHomedTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        if data[8] == 1:
            self._device._homed = True
        else:
            self._device._homed = False


class PtMeasTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class ScanOnCurveTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class ScanOnCircleTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        data = data.split(',')
        # Have to update the values like this (unless there is faster method) as to
        # not reassign the object that ...position points to, since that is the object
        # that _mea_pos of the tool references. So
        # self._device.active_tool.position = [data[0], data[1], data[2]] is not possible
        self._device.active_tool.position[0] = float(data[0])
        self._device.active_tool.position[1] = float(data[1])
        self._device.active_tool.position[2] = float(data[2])


class ClearAllErrorsTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass

    def on_complete(self):
        self._device._error = False
        super().on_complete()


class GoToTransaction(Transaction):
    """Orders the Server to perform a GoTo operation for the currently selected tool.
    """

    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class HomeTransaction(Transaction):
    """Orders the Server to home the currently selected tool.
    """

    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        pass


class EnumToolsTransaction(Transaction):
    def __init__(self, tag: str, device: Device):
        Transaction.__init__(self, tag, device)

    def update_device(self, data):
        data = data[1:-1]
        self._device.add_tool(data)
        # time.sleep(1)
        data = data.replace("(", "")
        data = data.replace(")", "")
        tool_json = soil_model['components'][0]['components'][0]['components'][0]
        tool_json['uuid'] = "COM-" + data

        requests.put("http://localhost:9002/COM-Cmm/COM-ToolChanger/COM-" + data,
                     json={
                         "class_name": "COMTool",
                         "json_file": tool_json,
                         "args": (data, "COM-" + data),
                         "kwargs": {}
                     })


def create(command: str, tag: str, device: Device) -> Transaction | None:
    module = modules[__name__]
    try:
        transaction = getattr(module, '{}Transaction'.format(command))(tag, device)
    except Exception:
        transaction = UnknownCommandTransaction(tag, device)
        print("Unknown command: " + '"' + command + '"')
        print("Response data will just be printed.")
    return transaction
