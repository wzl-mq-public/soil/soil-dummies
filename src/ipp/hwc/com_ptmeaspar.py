

from typing import List
from wzl.stream.job import Job
from wzl.soil.event import Event, EventTrigger, EventSeverity
from .device import Device
from scripts.gen.ipp.tool.com_ptmeaspar import COMPtMeasParTOP


class COMPtMeasPar(COMPtMeasParTOP):

    def __init__(self, device: Device, accel: float = 0, approach: float = 0, retract: float = 0, search: float = 0, speed: float = 0, owner: str = ""  ):
        COMPtMeasParTOP.__init__(self, device, accel, approach, retract, search, speed, owner)
        self._device = device
        self._par_owner = owner
        self._par_accel = accel
        self._par_approach = approach
        self._par_retract = retract
        self._par_search = search
        self._par_speed = speed

    def get_par_accel(self) -> float:
        return self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Accel']['value']['Act']['value']

    def set_par_accel(self, accel: float):
        self._device.tool_list[self._par_name].props['PtMeasPar']['value']['Accel']['value']['Act']['value'] = accel

    def get_par_approach(self) -> float:
        return self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Approach']['value']['Act']['value']

    def set_par_approach(self, approach: float):
        self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Approach']['value']['Act']['value'] = approach

    def get_par_retract(self) -> float:
        return self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Retract']['value']['Act']['value']

    def set_par_retract(self, retract: float):
        self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Retract']['value']['Act']['value'] = retract

    def get_par_search(self) -> float:
        return self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Search']['value']['Act']['value']

    def set_par_search(self, search: float):
        self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Search']['value']['Act']['value'] = search

    def get_par_speed(self) -> float:
        return self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Speed']['value']['Act']['value']

    def set_par_speed(self, speed: float):
        self._device.tool_list[self._par_owner].props['PtMeasPar']['value']['Speed']['value']['Act']['value'] = speed

    def get_par_owner(self) -> str:
        return self._par_owner

    def set_par_owner(self, owner: str):
        self._par_owner = owner
