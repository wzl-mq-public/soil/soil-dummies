

from typing import List
from wzl.stream.job import Job
from wzl.soil.event import Event, EventTrigger, EventSeverity
from .device import Device
from scripts.gen.ipp.tool.com_scanpar import COMScanParTOP


class COMScanPar(COMScanParTOP):

    def __init__(self, device: Device, accel: float = 0, retract: float = 0, speed: float = 0, owner: str = ""  ):
        COMScanParTOP.__init__(self, device, accel, retract, speed, owner)
        self._device = device
        self._par_accel = accel
        self._par_retract = retract
        self._par_speed = speed
        self._par_owner = owner

    def get_par_accel(self) -> float:
        return self._device.tool_list[self._par_owner].props['ScanPar']['value']['Accel']['value']['Act']['value']

    def set_par_accel(self, accel: float):
        self._device.tool_list[self._par_owner].props['ScanPar']['value']['Accel']['value']['Act']['value'] = accel

    def get_par_retract(self) -> float:
        return self._device.tool_list[self._par_owner].props['ScanPar']['value']['Retract']['value']['Act']['value']

    def set_par_retract(self, retract: float):
        self._device.tool_list[self._par_owner].props['ScanPar']['value']['Retract']['value']['Act']['value'] = retract

    def get_par_speed(self) -> float:
        return self._device.tool_list[self._par_owner].props['ScanPar']['value']['Speed']['value']['Act']['value']

    def set_par_speed(self, speed: float):
        self._device.tool_list[self._par_owner].props['ScanPar']['value']['Speed']['value']['Act']['value'] = speed

    def get_par_owner(self) -> str:
        return self._par_owner

    def set_par_owner(self, owner: str):
        self._par_owner = owner
