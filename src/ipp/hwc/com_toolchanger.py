

from typing import List
from wzl.soil.event import Event, EventTrigger, EventSeverity
from .device import Device
from scripts.gen.ipp.toolChanger.com_toolchanger import COMToolChangerTOP


class COMToolChanger(COMToolChangerTOP):

    def __init__(self, device: Device ):
        COMToolChangerTOP.__init__(self, device)
        self._device = device

    def fun_enumtools(self):
        # TODO implement
        pass

    def fun_changetool(self,arg_tool_id: str = ""):
        self._device.change_tool(arg_tool_id)
