from __future__ import annotations
import socket
import time
import threading
from threading import Thread
import typing
from . import transaction

if typing.TYPE_CHECKING:
    from .device import Device


class UnexpectedTagError(Exception):
    def __init__(self):
        self.message = "The received message contains an unexpected tag."


class MissingACKError(Exception):
    def __init__(self):
        self.message = "Was expecting an ACK response but received something else."


class MissingTransactionCompleteResponseError(Exception):
    def __init__(self):
        self.message = "Transaction complete response is missing."


class DaemonNonDataMessage(Exception):
    def __init__(self):
        self.message = "Received a daemon message that is not a data message!"


class IPPClient(Thread):
    def __init__(self, device_i: Device):
        Thread.__init__(self)
        self._socket = None
        self._active = False
        self._command_tag = 1
        self.transaction_log = {}
        self._device = device_i
        self._lock = threading.Lock()

    def _handle_event(self, tag, message_type, message_body):
        non_event_tag = "0" + tag[1:5]
        if non_event_tag in self._active_streams:
            daemon = self.transaction_log.get(non_event_tag)
            if message_type != "#":
                raise DaemonNonDataMessage()
            daemon['cb'].update_device(message_body.split(","))

    def _parse_message(self, message):
        print("<-- " + message)
        message = message.split(" ")
        tag = message[0]
        message_type = message[1]
        if tag[0] == "E":
            self._handle_event(tag, message_type, "".join(message[2:]))
            return

        if message_type == '!':
            # Should use the self._device._error flag and deal with it appropriately
            print(f'Server responded with an error for {tag}, sending "ClearAllErrors()"')
            self._device._error = True
            self.transaction("ClearAllErrors()", False)
            return

        transaction = self.transaction_log.get(tag)
        if not transaction['ACK']:
            if message_type == '&':
                transaction['ACK'] = True
                return
            else:
                raise MissingACKError()

        if message_type == '#':
            transaction['cb'].update_device(''.join(message[2::]))
            return

        if message_type == '%':
            blocking = transaction['blocking']
            transaction['cb'].on_complete()
            if blocking:
                self._lock.release()
            return

        return

    def _send(self, message):
        print("--> " + message.decode())
        self._socket.sendall(message)

    def transaction(self, command, blocking=False):
        tag = str(self._command_tag).zfill(5)
        command_name = command.split('(')[0]

        # Add transaction to pending transaction log
        self.transaction_log[tag] = {
            'ACK': False,
            'uuid': command,
            'cb': transaction.create(command_name, tag, self._device),
            'blocking': blocking
        }

        if blocking:
            self._lock.acquire()

        self._send(str.encode(tag + " " + command + "\r\n"))
        self._command_tag = self._command_tag + 1
        # Sleep for 1 second to not overwhelm the I++ Server
        time.sleep(0.1)
        while self._lock.locked():
            print("I++ currently blocked, while waiting for: " + self.transaction_log[tag]['uuid'])
            time.sleep(2)

    def _listen(self):
        buffer = []
        stitch = False
        while True:
            if (len(buffer) > 0 and not stitch) or (len(buffer) > 1):
                self._parse_message(buffer[0])
                buffer.pop(0)
                continue
            data, _ = self._socket.recvfrom(1024)
            data = data.decode()
            buffer = buffer + (data.splitlines())
            if stitch:
                buffer[0:2] = ["".join(buffer[0:2])]
                stitch = False
            if len(data) > 0:
                if data[-1] != '\n':
                    stitch = True

    def run(self):
        self._active = True
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect(('localhost', 1294))

        listener = Thread(target=self._listen)
        listener.start()
