from typing import List
from typing import Tuple
from typing import Any
from .com_gotopar import COMGoToPar
from .com_scanpar import COMScanPar
from .com_ptmeaspar import COMPtMeasPar
from .device import Device
from scripts.gen.ipp.tool.com_tool import COMToolTOP


class COMTool(COMToolTOP):

    def __init__(self, device: Device, name: str = "", id: str = "", alignment: str = "", avroffsets: str = "", avrradius: float = 0, init: bool = False  ):
        COMToolTOP.__init__(self, device, name, id, alignment, avroffsets, avrradius)
        self._device = device
        self._par_name = name
        self._par_init = init
        self._par_id = id
        self._par_alignment = alignment
        self._par_avroffsets = avroffsets
        self._par_avrradius = avrradius
        self._par_init = init
        self._com_gotopar = COMGoToPar(device, accel = 0, speed = 0, owner=self._par_name)
        self._com_ptmeaspar = COMPtMeasPar(device, accel = 0, approach = 0, retract = 0, search = 0, speed = 0, owner=self._par_name)
        self._mea_pos = self._device.tool_list[name].position if name != "" else [None, None, None]
        self._com_scanpar = COMScanPar(device, accel = 0, retract = 0, speed = 0, owner=self._par_name)

    def get_mea_pos(self) -> Tuple[List[float], Any]:
        return self._mea_pos, None

    def get_par_name(self) -> str:
        return self._par_name

    def get_par_id(self) -> str:
        return self._par_id

    def get_par_alignment(self) -> str:
        return self._device.tool_list[self._par_name].props['Alignment']['value']

    def set_par_alignment(self, alignment: str):
        self._device.tool_list[self._par_name].props['Alignment']['value'] = alignment

    def get_par_avroffsets(self) -> str:
        return self._device.tool_list[self._par_name].props['AvrOffsets']['value']

    def set_par_avroffsets(self, avroffsets: str):
        self._device.tool_list[self._par_name].props['AvrOffsets']['value'] = avroffsets

    def get_par_avrradius(self) -> float:
        return self._device.tool_list[self._par_name].props['AvrRadius']['value']

    def set_par_avrradius(self, avrradius: float):
        self._device.tool_list[self._par_name].props['AvrRadius']['value'] = avrradius

    def fun_ptmeas(self,arg_pos: List[float] = None):
        self._device.pt_meas(arg_pos[0], arg_pos[1], arg_pos[2])

    def fun_scanoncircle(self, arg_center: List[float] = None, arg_start: List[float] = None,arg_delta: float = 0,arg_surfaceangle: float = 0, arg_stepsize: int = 1):
        self._device.scan_on_circle(arg_center, arg_start, arg_delta, arg_surfaceangle, arg_stepsize)

    def fun_goto(self,arg_pos: List[float] = None):
        self._device.goto(arg_pos[0], arg_pos[1], arg_pos[2])

    def get_par_init(self) -> bool:
        return self._device.tool_list[self._par_name].initialized

    def set_par_init(self, init: bool):
        self._par_init = init
