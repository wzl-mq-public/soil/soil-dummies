import random
import time
from scripts.gen.ipp.device import DeviceTOP
from threading import Thread
from scripts.gen.ipp.hwc import ipp_client


class Tool(object):
    def __init__(self, name, device):
        self.props = {}
        self._name = name
        self.position = [0, 0, 0]
        self._device = device
        self.initialized = False

    def get_all_props(self, props=None, prop_name="Tool"):
        if props is None:
            props = self.props
        keys = props.keys()
        for k in keys:
            if props[k]['type'] == "Property":
                self.get_all_props(props[k]['value'], prop_name + '.' + k)
            else:
                self._device.ipp_client.transaction(f"GetProp({prop_name + '.' + k}())", False)

    def build_prop(self, key, props):
        keys = key.split('.')[1::]
        current_prop = self.props
        for k in keys:
            current_prop = current_prop[k]['value']

        current_prop.update(props)

    def set_prop(self, keys, value):
        current_prop = self.props
        keys = keys[1::]
        for i in range(len(keys) - 1):
            current_prop = current_prop[keys[i]]['value']

        if current_prop[keys[-1]]['type'] == "Number":
            current_prop[keys[-1]]['value'] = float(value)
        else:
            current_prop[keys[-1]]['value'] = value

    def init(self):
        print("---------------Building the Prop tree...---------------")
        self.init_props()
        print("---------------Building Prop tree finished.---------------")
        print("---------------Populating the Prop tree...---------------")
        self.get_all_props()
        self.initialized = True
        print("---------------Populating the Prop tree finished.---------------")

    def init_props(self, current_prop=None, prop_name="Tool"):
        if current_prop is None:
            self._device.ipp_client.transaction("EnumProp(Tool())", blocking=True)
            self.init_props(self.props)
            return

        # remaining_to_resolve = {k: v for k, v in current_prop if v['type'] == "Property"}
        remaining_to_resolve = []
        for k, v in current_prop.items():
            if v['type'] == "Property":
                remaining_to_resolve.append(k)

        for k in remaining_to_resolve:
            current_prop[k]['value'] = {}
            self._device.ipp_client.transaction(f"EnumProp({prop_name}.{k}())", blocking=True)
            self.init_props(current_prop[k]['value'], prop_name + '.' + k)

    @property
    def name(self):
        return self._name


class Device(DeviceTOP, Thread):
    def __init__(self):
        DeviceTOP.__init__(self)
        Thread.__init__(self)
        self._x = 0
        self._y = 0
        self._z = 0
        self.tool_list = {}
        self.active_tool = None
        self._error = False
        self.ipp_client = None
        self.position = [0, 0, 0]

    def add_tool(self, name):
        tool = Tool(name=name, device=self)
        name = name.replace("(", "")
        name = name.replace(")", "")
        if name not in self.tool_list.keys():
            self.tool_list[name] = tool

    def change_tool(self, tool: str):
        print(f'---------------Changing tool to {tool}...---------------')
        self.ipp_client.transaction(f'ChangeTool("{tool}")', blocking=True)
        tool = tool.replace("(", "")
        tool = tool.replace(")", "")
        self.active_tool = self.tool_list[tool]
        print("---------------Tool changed, initializing the tool...---------------")
        if not self.active_tool.initialized:
            self.active_tool.init()

    def goto(self, x, y, z):
        self.ipp_client.transaction("GoTo(X(" + str(x) + "), Y(" + str(y) + "), Z(" + str(z) + "))", blocking=True)
        self._x, self._y, self._z = x, y, z

    def home(self):
        self.ipp_client.transaction("Home()", True)

    def set_prop(self, keys, value):
        command = "SetProp(Tool"
        for key in keys[1:]:
            if key != "Act" and key != "Min" and key != "Max" and key != "Def":
                command = command + '.' + key

        command = command + f"({value}))"
        self.ipp_client.transaction(command, False)
        self.active_tool.set_prop(keys, value)

    def pt_meas(self, x, y, z):
        self.ipp_client.transaction("PtMeas(X(" + str(x) + "), Y(" + str(y) + "), Z(" + str(z) + "))", False)

    def scan_on_circle(self, center_point, start_point, delta, surface_angle, step_size):
        self.ipp_client.transaction(f"ScanOnCircle("
                                     f"{str(center_point[0])},"
                                     f"{str(center_point[1])},"
                                     f"{str(center_point[2])},"
                                     f"{str(start_point[0])},"
                                     f"{str(start_point[1])},"
                                     f"{str(start_point[2])},"
                                     f"0, 0, 1,"
                                     f"{delta},"
                                     f"{surface_angle}, "
                                     f"{step_size}, 0)", False)

    def custom_commands(self):
        current_command = ""
        while current_command != "exit":
            current_command = input("Type in command...")
            self.ipp_client.transaction(current_command, True)

    def tool_test(self):
        for tool in self.tool_list:
            self.change_tool(tool.name)

    def bug_script(self):
        self.ipp_client.transaction('ChangeTool("PRB(1)")', True)
        # time.sleep(2)
        self.ipp_client.transaction("Home()", True)
        for i in range(1000):
            x = random.randint(0, 10)
            y = random.randint(0, 10)
            z = random.randint(0, 10)
            self.ipp_client.transaction(f'GoTo(X({x}), Y({y}), Z({z}))', True)

    def init(self):
        """Initial code that is executed on startup of the interface.

        A session is started and the test tool "PRB(1)" is selected.
        """
        self.ipp_client.transaction("StartSession()", False)
        self.ipp_client.transaction("Home()", True)
        self.ipp_client.transaction("EnumTools()", True)

    def run(self):
        self.ipp_client = ipp_client.IPPClient(self)
        self.ipp_client.start()
        time.sleep(1)

        self.init()

        # self.change_tool("PRB(1)")
        # self.set_prop(["Tool", "GoToPar", "Speed"], 15)
        # self.set_prop(["Tool", "PtMeasPar", "Approach", "Act"], 20)
        # self.set_prop(["Tool", "PtMeasPar", "Search", "Act"], 10)
        # self.set_prop(["Tool", "PtMeasPar", "Retract", "Act"], 35)
        # self.goto(0, 0, 150)
        # self.goto(100, 100, 150)
        # self.scan_on_circle([100, 100, 150], [100, 150, 150], 360, 0, 1)

        # self.custom_commands()

    def stop(self):
        self.ipp_client.transaction('EndSession()', False)
