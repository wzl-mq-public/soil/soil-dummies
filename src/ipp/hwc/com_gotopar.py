

from typing import List
from wzl.soil.event import Event, EventTrigger, EventSeverity
from wzl.stream.job import Job
from .device import Device
from scripts.gen.ipp.tool.com_gotopar import COMGoToParTOP


class COMGoToPar(COMGoToParTOP):

    def __init__(self, device: Device, accel: float = 0, speed: float = 0, owner: str = "" ):
        COMGoToParTOP.__init__(self, device, accel, speed, owner)
        self._device = device
        self._par_accel = accel
        self._par_speed = speed
        self._par_owner = owner

    def get_par_accel(self) -> float:
        return self._device.tool_list[self._par_owner].props['GoToPar']['value']['Accel']['value']['Act']['value']

    def set_par_accel(self, accel: float):
        self._device.tool_list[self._par_owner].props['GoToPar']['value']['Accel']['value']['Act']['value'] = accel

    def get_par_speed(self) -> float:
        return self._device.tool_list[self._par_owner].props['GoToPar']['value']['Speed']['value']['Act']['value']

    def set_par_speed(self, speed: float):
        self._device.tool_list[self._par_owner].props['GoToPar']['value']['Speed']['value']['Act']['value'] = speed

    def get_par_owner(self) -> str:
        return self._par_owner

    def set_par_owner(self, owner: str):
        self._par_owner = owner
