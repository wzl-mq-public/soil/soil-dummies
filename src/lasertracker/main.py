import toml

import start
from hwc.device import Device
from hwc.com_lasertracker import COMLasertracker

if __name__ == '__main__':
    device = Device()
    lasertracker = COMLasertracker(device)
    device.start()

    start.start(lasertracker, toml.load('config.toml'), 'model.json')
