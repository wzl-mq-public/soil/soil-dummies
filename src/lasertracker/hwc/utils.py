import math


def radian(angle):
    return angle / 180 * math.pi


def degree(angle):
    return angle / math.pi * 180


def cartesian(distance, azimuth, elevation):
    x = distance * math.cos(elevation) * math.cos(azimuth)
    y = distance * math.cos(elevation) * math.sin(azimuth)
    z = distance * math.sin(elevation)
    return [x, y, z]


def spherical(x, y, z):
    distance = math.sqrt(x ** 2 + y ** 2 + z ** 2)
    azimuth = math.atan2(y, x)
    elevation = math.asin(z / distance)
    return [distance, azimuth, elevation]
