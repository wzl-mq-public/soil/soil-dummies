import math
import random

from base_stations.com_base import COMBaseTOP
from hwc.device import Device
from utils.enum_state import State


class COMBase(COMBaseTOP):

    def __init__(self, device: Device, state: State = None, interval: float = 0):
        COMBaseTOP.__init__(self, device, state, interval)
        self._device = device
        self._var_position = [random.random() * 10 - 5, random.random() * 10 - 5, random.random() * 10 - 5]
        self._var_quaternion = COMBase._to_quaternion(random.random() * 2 * math.pi - math.pi,
                                                      random.random() * 2 * math.pi - math.pi,
                                                      random.random() * 2 * math.pi - math.pi)

    def fun_jog(self, par_azimuth=0, par_elevation=0):
        self._device.jog(par_azimuth, par_elevation)

    def fun_pointto(self, par_position=None):
        par_position = [0, 0, 0] if par_position is None else par_position
        self._device.point_to(*par_position)

    def get_mea_azimuth(self):
        return self._device.azimuth, random.random() * 10e-10

    def get_mea_elevation(self):
        return self._device.elevation, random.random() * 10e-10

    def get_mea_distance(self):
        return self._device.distance, random.random() * 10e-9

    def get_par_state(self):
        return str(self._device.state)

    @staticmethod
    def _to_quaternion(yaw, pitch, roll):
        cy = math.cos(yaw * 0.5)
        sy = math.sin(yaw * 0.5)
        cp = math.cos(pitch * 0.5)
        sp = math.sin(pitch * 0.5)
        cr = math.cos(roll * 0.5)
        sr = math.sin(roll * 0.5)

        w = cy * cp * cr + sy * sp * sr
        x = cy * cp * sr - sy * sp * cr
        y = sy * cp * sr + cy * sp * cr
        z = sy * cp * cr - cy * sp * sr

        return [w, x, y, z]
