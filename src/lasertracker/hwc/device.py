import random
import time
from threading import Thread

from hwc import utils
from mobile_entities.enum_mode import Mode
from utils.enum_state import State
from device import DeviceTOP

AZIMUTH_MIN = utils.radian(-320)
AZIMUTH_MAX = utils.radian(320)
ELEVATION_MIN = utils.radian(-59)
ELEVATION_MAX = utils.radian(79)
DISTANCE_MIN = 0
DISTANCE_MAX = 80


class CriticalLaserTrackerError(Exception):

    def __init__(self):
        self.message = 'A critical error occurred. Please shutdown the tracker and restart the server!'


class LaserTrackerError(Exception):

    def __init__(self):
        self.message = 'The device is in "Error"-State. Please reset or restart the tracker!'


class TargetError(Exception):

    def __init__(self):
        self.message = "Position of target can not be measured."


class Target(object):

    def __init__(self, position):
        self._position = position
        self._state = State.OK
        self._mode = Mode.CONTINUOUS

    def update(self, position=None, mode=Mode.CONTINUOUS, state=State.OK):
        if position is not None:
            self._position = position
        self._mode = mode
        self._state = state

    @property
    def position(self):
        if self.mode == Mode.IDLE or self._state in [State.ERROR, State.MAINTENANCE]:
            raise TargetError()
        return self._position

    @property
    def mode(self):
        return self._mode

    @property
    def state(self):
        return self._state


class Device(DeviceTOP, Thread):

    def __init__(self):
        DeviceTOP.__init__(self)
        Thread.__init__(self)
        self._azimuth = 0
        self._elevation = 0
        self._distance = 0
        self._state = State.OK
        self._target = Target(self.cartesian)
        self._critical_error = False

    @property
    def target(self):
        return self._target

    @property
    def state(self):
        return self._state

    @property
    def azimuth(self):
        if self._state != State.ERROR:
            return self._azimuth
        else:
            raise LaserTrackerError()

    @property
    def elevation(self):
        if self._state != State.ERROR:
            return self._elevation
        else:
            raise LaserTrackerError()

    @property
    def distance(self):
        if self._state != State.ERROR and self._target.state == State.OK and self._target.mode != Mode.IDLE:
            return self._distance
        else:
            raise LaserTrackerError()

    @property
    def spherical(self):
        if self._state != State.ERROR:
            return [self._distance, self._azimuth, self._elevation]
        else:
            raise LaserTrackerError()

    @property
    def cartesian(self):
        if self._state != State.ERROR:
            return utils.cartesian(self._distance, self._azimuth, self._elevation)
        else:
            raise LaserTrackerError()

    def jog(self, azimuth, elevation):
        if self._state != State.ERROR:
            self._azimuth += azimuth
            self._elevation += elevation
            self._target.update(state=State.ERROR, mode=Mode.IDLE)
        else:
            raise LaserTrackerError()

    def point_to(self, x, y, z):
        if self._state != State.ERROR:
            spherical = utils.spherical(x, y, z)
            self._azimuth = spherical[1]
            self._elevation = spherical[2]
        else:
            raise LaserTrackerError()

    def reset_target(self):
        if self._state != State.ERROR:
            self._azimuth = random.uniform(AZIMUTH_MIN, AZIMUTH_MAX)
            self._elevation = random.uniform(ELEVATION_MIN, ELEVATION_MAX)
            self._distance = random.uniform(DISTANCE_MIN, DISTANCE_MAX)
            self._target.update(self.cartesian, mode=Mode.CONTINUOUS, state=State.OK)
        else:
            raise LaserTrackerError()

    def reset_tracker(self):
        if random.random() > 0.1 and not self._critical_error:
            self._azimuth = random.uniform(AZIMUTH_MIN, AZIMUTH_MAX)
            self._elevation = random.uniform(ELEVATION_MIN, ELEVATION_MAX)
            self._distance = random.uniform(DISTANCE_MIN, DISTANCE_MAX)
        else:
            self._critical_error = True
            raise CriticalLaserTrackerError()

    def run(self):
        while self._state != State.ERROR:
            # a little bit of fluke against boredom
            fluke = random.random()
            try:
                if fluke < 0.0005:
                    pass
                    # self._state = State.ERROR
                    # self._target.update(state=State.ERROR)
                elif fluke < 0.01:
                    pass
                    # self._target.update(mode=Mode.IDLE, state=State.ERROR)
                elif fluke < 0.15:
                    self._state = State.WARNING
                elif fluke < 0.3:
                    self._target.update(state=State.WARNING)

                if fluke > 0.75 and self.state == State.WARNING:
                    self._state = State.OK

                if fluke > 0.25 and self._target.state == State.WARNING:
                    self._target.update(state=State.OK)

                self._azimuth += random.random() - 0.5
                if self._azimuth < AZIMUTH_MIN:
                    self._azimuth = AZIMUTH_MIN
                elif self._azimuth > AZIMUTH_MAX:
                    self._azimuth = AZIMUTH_MAX
                self._elevation += random.random() - 0.5
                if self._elevation < ELEVATION_MIN:
                    self._elevation = ELEVATION_MIN
                elif self._elevation > ELEVATION_MAX:
                    self._elevation = ELEVATION_MAX
                self._distance += random.random() * 2 - 1
                if self._distance < DISTANCE_MIN:
                    self._distance = DISTANCE_MIN
                elif self._distance > DISTANCE_MAX:
                    self._distance = DISTANCE_MAX
                self._target.update(self.cartesian)
            except Exception as e:
                pass

            time.sleep(5) # (0.1)

    def stop(self):
        self._state = State.ERROR
        self._critical_error = True
