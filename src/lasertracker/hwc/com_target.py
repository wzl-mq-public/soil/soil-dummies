import random

import time
from typing import List

from hwc.device import Device
from mobile_entities.com_target import COMTargetTOP
from mobile_entities.enum_mode import Mode
from utils.enum_state import State


class COMTarget(COMTargetTOP):

    def __init__(self, device: Device, state: State = None, mode: Mode = None, type: str = ""):
        COMTargetTOP.__init__(self, device, state, mode, type)

    def fun_reset(self):
        self._device.reset_target()

    def fun_trigger(self, arg_counter: int = 100, arg_label: str = "test") -> List[float]:
        for i in range(arg_counter):
            time.sleep(0.1)
            yield self.get_mea_position(), arg_label

    def get_mea_position(self):
        return self._device.target.position, [[random.random()*10e-9 for j in range(3)] for i in range(3)]

    def get_par_state(self):
        return str(self._device.target.state)

    def get_par_mode(self):
        return str(self._device.target.mode)
