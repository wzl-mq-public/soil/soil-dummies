from com_environmentalsensormanager import COMEnvironmentalSensorManagerTOP
from hwc.device import Device
from hwc.com_environmentalsensor import COMEnvironmentalSensor


class COMEnvironmentalSensorManager(COMEnvironmentalSensorManagerTOP):

    def __init__(self, device: Device):
        COMEnvironmentalSensorManagerTOP.__init__(self, device)
        self._com_environmentalsensor = {}
        self._com_environmentalsensor['COM-Sensor'] = COMEnvironmentalSensor(device, location="Pillar 1",
                                                                             id='COM-Sensor')

    def add(self, uuid: str, child: COMEnvironmentalSensor):
        super().add(uuid, child)
        self._device.add(uuid)

    def remove(self, uuid: str):
        super().remove(uuid)
        self._device.remove(uuid)
