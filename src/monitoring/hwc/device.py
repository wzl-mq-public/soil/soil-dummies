import random
import threading
import time

from device import DeviceTOP

class BatteryEmptyException(Exception):
    pass


class SensorDevice(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self._temperature = random.random() * 70 - 20
        self._humidity = random.random() * 100
        self._pressure = 900 + random.random() * 300
        self._battery_level = 100
        self._signal_strength = random.randint(1, 101)
        self.running = True

    def run(self) -> None:
        while self.running:
            self._temperature = self._temperature + random.random() * 4 - 2
            if self._temperature < -20:
                self._temperature = -20
            elif self._temperature > 70:
                self._temperature = 70

            self._humidity = self._humidity + random.random() * 8 - 4
            if self._humidity < 2:
                self._humidity = 2
            elif self._humidity > 98:
                self._humidity = 98

            self._pressure = self._pressure + random.random() * 50 - 25
            if self._pressure < 900:
                self._pressure = 900
            elif self._pressure > 1200:
                self._pressure = 1200

            self._signal_strength = random.randint(1, 101)
            self._battery_level -= 0.1

            # Keep the device running forever
            if self._battery_level < 2:
                self._battery_level = 100

            time.sleep(2)

    @property
    def temperature(self) -> float:
        if self._battery_level < 0:
            raise BatteryEmptyException()
        return self._temperature

    @property
    def humidity(self) -> float:
        if self._battery_level < 0:
            raise BatteryEmptyException()
        return self._humidity

    @property
    def pressure(self) -> float:
        if self._battery_level < 0:
            raise BatteryEmptyException()
        return self._pressure

    @property
    def signal_strength(self) -> float:
        if self._battery_level < 0:
            raise BatteryEmptyException()
        return self._signal_strength

    @property
    def battery_level(self) -> float:
        if self._battery_level < 0:
            raise BatteryEmptyException()
        return self._battery_level


class Device(DeviceTOP):

    def __init__(self):
        DeviceTOP.__init__(self)
        self.sensors = {'COM-Sensor': SensorDevice()}
        for sensor in self.sensors.values():
            sensor.start()

    def __del__(self):
        pass

    def add(self, uuid: str):
        self.sensors[uuid] = SensorDevice()
        self.sensors[uuid].start()

    def remove(self, uuid: str):
        self.sensors[uuid].running = False
        del self.sensors[uuid]
