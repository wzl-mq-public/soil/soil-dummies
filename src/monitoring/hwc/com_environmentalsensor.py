from typing import Tuple, Any

import random

from com_environmentalsensor import COMEnvironmentalSensorTOP
from hwc.device import Device


class COMEnvironmentalSensor(COMEnvironmentalSensorTOP):

    def __init__(self, device: Device, location: str = "", id: str = ""):
        COMEnvironmentalSensorTOP.__init__(self, device, location)
        self.uuid = id
        
    def get_mea_temperature(self) -> Tuple[float, Any]:
        return self._device.sensors[self.uuid].temperature, random.random() * 10e-2

    def get_mea_pressure(self) -> Tuple[float, Any]:
        return self._device.sensors[self.uuid].pressure, random.random() * 10e-1

    def get_mea_humidity(self) -> Tuple[float, Any]:
        return self._device.sensors[self.uuid].humidity, random.random() * 10e-3

    def get_mea_signalstrength(self) -> Tuple[int, Any]:
        return self._device.sensors[self.uuid].signal_strength, None

    def get_mea_batterylevel(self) -> Tuple[int, Any]:
        return self._device.sensors[self.uuid].battery_level, None

    def get_par_location(self) -> str:
        return self._par_location

    def set_par_location(self, location: str):
        self._par_location = location
