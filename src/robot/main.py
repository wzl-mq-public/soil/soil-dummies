import toml

import start
from hwc.device import Device
from hwc.com_mobilerobot import COMMobileRobot

if __name__ == '__main__':
    device = Device()
    wallE = COMMobileRobot(device)

    start.start(wallE, toml.load('config.toml'), 'model.json')