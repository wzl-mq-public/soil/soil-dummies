from typing import List, Any, Tuple

import numpy

from com_mobilerobot import COMMobileRobotTOP
from hwc.device import Device, Mode


class COMMobileRobot(COMMobileRobotTOP):

    def __init__(self, device: Device, auto: bool = True):
        COMMobileRobotTOP.__init__(self, device, auto)
        self._mea_position = numpy.array([0, 0, 0], dtype=float)
        self._device.start()

    def get_mea_position(self) -> Tuple[List[float], Any]:
        return self._device.position.tolist(), None

    def get_mea_batterylevel(self) -> Tuple[int, Any]:
        return self._device.batterylevel, None

    def get_par_auto(self) -> bool:
        return self._device.mode == Mode.AUTO

    def set_par_auto(self, auto: bool):
        self._device.mode = Mode.AUTO if auto else Mode.IDLE

    def fun_gotorobot(self, arg_position: List[float] = None):
        self._device.goto(numpy.array(arg_position))

    def fun_steprobot(self, arg_position: List[float] = None):
        self._device.step(numpy.array(arg_position))
