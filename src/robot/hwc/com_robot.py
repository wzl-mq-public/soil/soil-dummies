import time
from typing import List, Tuple, Any

import numpy

from hwc.com_gripper import COMGripper
from com_robot import COMRobotTOP
from hwc.device import Device


class COMRobot(COMRobotTOP):

    def __init__(self, device: Device):
        COMRobotTOP.__init__(self, device)
        self._device = device
        self._mea_position = numpy.array([0, 0, 0], dtype=float)
        self._com_gripper = COMGripper(device, open=True)

    def get_mea_position(self) -> Tuple[List[float], Any]:
        return self._device.position.tolist(), None

    def fun_goto(self, arg_position: List[float] = None):
        self._device.goto(numpy.array(arg_position))

    def fun_step(self, arg_position: List[float] = None):
        self._device.step(numpy.array(arg_position))
