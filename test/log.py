import os.path

import datetime

import logging


def get(name: str, path: str) -> logging.Logger:
    logger = logging.getLogger(f'{name}.py')
    logger.setLevel(logging.DEBUG)
    formatter = logging.Formatter('### %(levelname)-8s %(asctime)s  %(name)-40s ###\n%(message)s\n')

    file_handler = logging.FileHandler(os.path.join(path, f'{name}_{datetime.datetime.now().isoformat().replace(":", "-")[:-7]}.log'), mode='w')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)
    logger.addHandler(file_handler)

    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)
    console_handler.setLevel(logging.INFO)
    logger.addHandler(console_handler)

    return logger
