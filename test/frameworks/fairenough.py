import copy
import json
import os
import pandas
import requests
import toml
from tqdm import tqdm

from frameworks.framework import Framework, AssessmentError, mergedicts

config = toml.load('config.toml')


class FAIREnough(Framework):
    _base_url = f'https://purl.org/fair-sensor-services/'
    _request_url = 'https://api.fair-enough.semanticscience.org/evaluations'
    _token = config['fair-enough_token']
    _available_collections = ['fair-enough-data', 'fair-enough-metadata', 'fair-evaluator-maturity-indicators']

    def __init__(self, prefix: str, collection: 'str'):
        Framework.__init__(self, f'{self._base_url}{prefix}/')
        self._all_scores = pandas.DataFrame(
            columns=["framework", "dummy", "metric", "value"])
        if collection in self._available_collections:
            self._collection = collection
        else:
            raise ValueError()

    @property
    def name(self) -> str:
        return f'fair-enough_{self._collection}'

    def init_assessment(self, base_path: str, dummy: str, mode: str):
        if not os.path.exists(os.path.join(base_path, self.name)):
            os.mkdir(os.path.join(base_path, self.name))

        if not os.path.exists(os.path.join(base_path, self.name, dummy)):
            os.mkdir(os.path.join(base_path, self.name, dummy))

        if not os.path.exists(os.path.join(base_path, self.name, dummy, mode)):
            os.mkdir(os.path.join(base_path, self.name, dummy, mode))

        self._output_dir = os.path.join(base_path, self.name, dummy, mode)

    def assess(self, identifier: str):
        body = {
            'subject': f'{self._prefix_url}{identifier}',
            'collection': self._collection
        }

        response = requests.post(f"{self._request_url}/", json=body)

        if response.status_code < 300:
            json.dump(json.loads(response.content.decode('UTF-8')),
                      open(os.path.join(self._output_dir, f'{identifier}.json'), 'w'))
        else:
            raise AssessmentError(response.reason)

        return json.loads(response.content.decode('UTF-8'))

    def evaluate(self, assessment_folder: str, dummy: str, mode: str) -> pandas.DataFrame:
        directory = os.path.join(assessment_folder, self.name, dummy, mode)

        for assessment_filename in tqdm(os.listdir(directory)):
            with open(os.path.join(directory, assessment_filename)) as assessment_file:
                assessment = json.load(assessment_file)

            for key in assessment['contains']:
                entry = assessment['contains'][key]
                self._all_scores.loc[len(self._all_scores.index)] = [self.name, dummy, key.split('/')[-1], entry[0][
                    'http://semanticscience.org/resource/SIO_000300'][0]['@value']]

        average_scores = self._all_scores.groupby('metric', as_index=False).agg({
            'framework': 'first',
            'dummy': 'first',
            'value': 'mean'
        })

        return average_scores

    def convert(self) -> pandas.DataFrame:
        dummies = self._all_scores['dummy'].unique()
        assert len(dummies) == 1
        dummy = dummies[0]

        average_scores = self._all_scores.groupby('metric', as_index=False).agg({
            'framework': 'first',
            'dummy': 'first',
            'value': 'mean'
        })

        fuji_scores = pandas.DataFrame(columns=["framework", "dummy", "metric", "value"])
        fuji_mapping = {'A': 0, 'F': 0, 'I': 0, 'R': 0, 'A1': 0, 'A2': 0, 'F1': 0, 'F2': 0, 'F3': 0, 'F4': 0,
                        'I1': 0, 'I2': 0, 'I3': 0, 'R1': 0, 'R1.1': 0, 'R1.2': 0, 'R1.3': 0,
                        'FAIR': 0}

        if self._collection == 'fair-enough-metadata':
            for index in average_scores.index:
                # conversion formula: fuji_score = fair_checker_score / #metrics / scale_maximum * percentage_conversion
                fuji_mapping['FAIR'] += average_scores['value'][index] / 16 / 1 * 100
                if average_scores['metric'][index] in ['f1-metadata-identifier-unique',
                                                       'f1-metadata-identifier-persistent']:
                    fuji_mapping['F1'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 6 / 1 * 100
                elif average_scores['metric'][index] in ['f2-grounded-metadata', 'f2-structured-metadata']:
                    fuji_mapping['F2'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 6 / 1 * 100
                elif average_scores['metric'][index] in ['f3-metadata-identifier-in-metadata']:
                    fuji_mapping['F3'] += average_scores['value'][index] / 1 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 6 / 1 * 100
                elif average_scores['metric'][index] in ['f4-searchable']:
                    fuji_mapping['F4'] += average_scores['value'][index] / 1 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 6 / 1 * 100
                elif average_scores['metric'][index] in ['a1-metadata-authorization', 'a1-metadata-protocol']:
                    fuji_mapping['A1'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['A'] += average_scores['value'][index] / 3 / 1 * 100
                elif average_scores['metric'][index] in ['a2-metadata-persistent']:
                    fuji_mapping['A2'] += average_scores['value'][index] / 1 / 1 * 100
                    fuji_mapping['A'] += average_scores['value'][index] / 3 / 1 * 100
                elif average_scores['metric'][index] in ['i1-metadata-knowledge-representation-semantic',
                                                         'i1-metadata-knowledge-representation-structured']:
                    fuji_mapping['I1'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['I'] += average_scores['value'][index] / 5 / 1 * 100
                elif average_scores['metric'][index] in ['i2-fair-vocabularies-known', 'i2-fair-vocabularies-resolve']:
                    fuji_mapping['I2'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['I'] += average_scores['value'][index] / 5 / 1 * 100
                elif average_scores['metric'][index] in ['i3-metadata-contains-outward-links']:
                    fuji_mapping['I3'] += average_scores['value'][index] / 1 / 1 * 100
                    fuji_mapping['I'] += average_scores['value'][index] / 5 / 1 * 100
                elif average_scores['metric'][index] in ['r1-includes-standard-license', 'r1-includes-license']:
                    fuji_mapping['R1.1'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['R'] += average_scores['value'][index] / 2 / 1 * 100
        elif self._collection == 'fair-enough-data':
            for index in average_scores.index:
                # conversion formula: fuji_score = fair_checker_score / #metrics / scale_maximum * percentage_conversion
                fuji_mapping['FAIR'] += average_scores['value'][index] / 22 / 1 * 100
                if average_scores['metric'][index] in ['f1-metadata-identifier-unique',
                                                       'f1-metadata-identifier-persistent',
                                                       'f1-data-identifier-persistent']:
                    fuji_mapping['F1'] += average_scores['value'][index] / 3 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 8 / 1 * 100
                elif average_scores['metric'][index] in ['f2-grounded-metadata', 'f2-structured-metadata']:
                    fuji_mapping['F2'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 8 / 1 * 100
                elif average_scores['metric'][index] in ['f3-metadata-identifier-in-metadata',
                                                         'f3-data-identifier-in-metadata']:
                    fuji_mapping['F3'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 8 / 1 * 100
                elif average_scores['metric'][index] in ['f4-searchable']:
                    fuji_mapping['F4'] += average_scores['value'][index] / 1 / 1 * 100
                    fuji_mapping['F'] += average_scores['value'][index] / 8 / 1 * 100
                elif average_scores['metric'][index] in ['a1-metadata-authorization', 'a1-metadata-protocol',
                                                         'a1-data-protocol', 'a1-data-authorization']:
                    fuji_mapping['A1'] += average_scores['value'][index] / 4 / 1 * 100
                    fuji_mapping['A'] += average_scores['value'][index] / 5 / 1 * 100
                elif average_scores['metric'][index] in ['a2-metadata-persistent']:
                    fuji_mapping['A2'] += average_scores['value'][index] / 1 / 1 * 100
                    fuji_mapping['A'] += average_scores['value'][index] / 5 / 1 * 100
                elif average_scores['metric'][index] in ['i1-metadata-knowledge-representation-semantic',
                                                         'i1-metadata-knowledge-representation-structured',
                                                         'i1-data-knowledge-representation-semantic',
                                                         'i1-data-knowledge-representation-structured']:
                    fuji_mapping['I1'] += average_scores['value'][index] / 4 / 1 * 100
                    fuji_mapping['I'] += average_scores['value'][index] / 7 / 1 * 100
                elif average_scores['metric'][index] in ['i2-fair-vocabularies-known', 'i2-fair-vocabularies-resolve']:
                    fuji_mapping['I2'] += average_scores['value'][index] / 4 / 1 * 100
                    fuji_mapping['I'] += average_scores['value'][index] / 7 / 1 * 100
                elif average_scores['metric'][index] in ['i3-metadata-contains-outward-links']:
                    fuji_mapping['I3'] += average_scores['value'][index] / 1 / 1 * 100
                    fuji_mapping['I'] += average_scores['value'][index] / 7 / 1 * 100
                elif average_scores['metric'][index] in ['r1-includes-standard-license', 'r1-includes-license']:
                    fuji_mapping['R1.1'] += average_scores['value'][index] / 2 / 1 * 100
                    fuji_mapping['R'] += average_scores['value'][index] / 2 / 1 * 100
            # elif average_scores['metric'][index] in ['R1.2']:
            #     fuji_mapping['R1.2'] += average_scores['value'][index] / 1 / 2 * 100
            #     fuji_mapping['R'] += average_scores['value'][index] / 3 / 2 * 100
            # elif average_scores['metric'][index] in ['R1.3']:
            #     fuji_mapping['R1.3'] += average_scores['value'][index] / 1 / 2 * 100
            #     fuji_mapping['R'] += average_scores['value'][index] / 3 / 2 * 100

        for key in fuji_mapping:
            fuji_scores.loc[len(fuji_scores.index)] = [self.name, dummy, key, fuji_mapping[key]]

        average_scores = \
            fuji_scores[fuji_scores['metric'].isin(['F', 'A', 'I', 'R'])].groupby('metric', as_index=False).agg({
                'framework': 'first',
                'dummy': 'first',
                'value': 'mean'
            })

        return average_scores

    def visualize(self, scores: pandas.DataFrame, filename: str, config: dict):
        _config = dict(
            radialaxis=dict(
                range=[0, 2],
                tickvals=[0, 0.5, 1, 1.5, 2],
                ticklen=2,  # Set length of radial ticks
            ),
        )

        conf = copy.deepcopy(_config)
        mergedicts(conf, config)

        self._visualize(scores, filename, conf)
