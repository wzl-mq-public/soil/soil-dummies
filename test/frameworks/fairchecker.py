import copy
import json
import os
import pandas
import requests
from tqdm import tqdm

from frameworks.framework import Framework, AssessmentError, mergedicts


class FAIRChecker(Framework):
    _base_url = f'https://purl.org/fair-sensor-services/'
    _request_url = 'https://fair-checker.france-bioinformatique.fr/api/check/metrics_all'

    def __init__(self, prefix: str):
        Framework.__init__(self, f'{self._base_url}{prefix}/')
        self._all_scores = pandas.DataFrame(
            columns=["framework", "dummy", "metric", "value"])
        self._output_dir = None

    @property
    def name(self) -> str:
        return 'fair-checker'

    def assess(self, identifier: str):
        assert self._output_dir is not None

        response = requests.get(f"{self._request_url}?url={self._prefix_url}{identifier}/")

        if response.status_code == 200:
            json.dump(json.loads(response.content.decode('UTF-8')), open(os.path.join(self._output_dir, f'{identifier}.json'), 'w'))
        else:
            raise AssessmentError(response.reason)

        return json.loads(response.content.decode('UTF-8'))

    def evaluate(self, assessment_folder: str, dummy: str, mode: str) -> pandas.DataFrame:
        directory = os.path.join(assessment_folder, self.name, dummy, mode)

        for assessment_filename in tqdm(os.listdir(directory)):
            with open(os.path.join(directory, assessment_filename)) as assessment_file:
                assessment = json.load(assessment_file)

            for entry in assessment:
                self._all_scores.loc[len(self._all_scores.index)] = [self.name, dummy, entry['metric'],
                                                                     eval(entry['score'])]

        average_scores = self._all_scores.groupby('metric', as_index=False).agg({
            'framework': 'first',
            'dummy': 'first',
            'value': 'mean'
        })

        return average_scores

    def convert(self) -> pandas.DataFrame:
        dummies = self._all_scores['dummy'].unique()
        assert len(dummies) == 1
        dummy = dummies[0]

        average_scores = self._all_scores.groupby('metric', as_index=False).agg({
            'framework': 'first',
            'dummy': 'first',
            'value': 'mean'
        })

        fuji_scores = pandas.DataFrame(columns=["framework", "dummy", "metric", "value"])
        fuji_mapping = {'A': 0, 'F': 0, 'I': 0, 'R': 0, 'A1': 0, 'A2': 0, 'F1': 0, 'F2': 0, 'F3': 0, 'F4': 0,
                        'I1': 0, 'I2': 0, 'I3': 0, 'R1': 0, 'R1.1': 0, 'R1.2': 0, 'R1.3': 0,
                        'FAIR': 0}

        for index in average_scores.index:
            # conversion formula: fuji_score = fair_checker_score / #metrics / scale_maximum * percentage_conversion
            fuji_mapping['FAIR'] += average_scores['value'][index] / 12 / 2 * 100
            if average_scores['metric'][index] in ['F1A', 'F1B']:
                fuji_mapping['F1'] += average_scores['value'][index] / 2 / 2 * 100
                fuji_mapping['F'] += average_scores['value'][index] / 4 / 2 * 100
            elif average_scores['metric'][index] in ['F2A', 'F2B']:
                fuji_mapping['F2'] += average_scores['value'][index] / 2 / 2 * 100
                fuji_mapping['F'] += average_scores['value'][index] / 4 / 2 * 100
            elif average_scores['metric'][index] in ['A1.1', 'A1.2']:
                fuji_mapping['A1'] += average_scores['value'][index] / 2 / 2 * 100
                fuji_mapping['A'] += average_scores['value'][index] / 2 / 2 * 100
            elif average_scores['metric'][index] in ['I1']:
                fuji_mapping['I1'] += average_scores['value'][index] / 1 / 2 * 100
                fuji_mapping['I'] += average_scores['value'][index] / 3 / 2 * 100
            elif average_scores['metric'][index] in ['I2']:
                fuji_mapping['I2'] += average_scores['value'][index] / 1 / 2 * 100
                fuji_mapping['I'] += average_scores['value'][index] / 3 / 2 * 100
            elif average_scores['metric'][index] in ['I3']:
                fuji_mapping['I3'] += average_scores['value'][index] / 1 / 2 * 100
                fuji_mapping['I'] += average_scores['value'][index] / 3 / 2 * 100
            elif average_scores['metric'][index] in ['R1.1']:
                fuji_mapping['R1.1'] += average_scores['value'][index] / 1 / 2 * 100
                fuji_mapping['R'] += average_scores['value'][index] / 3 / 2 * 100
            elif average_scores['metric'][index] in ['R1.2']:
                fuji_mapping['R1.2'] += average_scores['value'][index] / 1 / 2 * 100
                fuji_mapping['R'] += average_scores['value'][index] / 3 / 2 * 100
            elif average_scores['metric'][index] in ['R1.3']:
                fuji_mapping['R1.3'] += average_scores['value'][index] / 1 / 2 * 100
                fuji_mapping['R'] += average_scores['value'][index] / 3 / 2 * 100

        for key in fuji_mapping:
            fuji_scores.loc[len(fuji_scores.index)] = [self.name, dummy, key, fuji_mapping[key]]

        average_scores = \
            fuji_scores[fuji_scores['metric'].isin(['F', 'A', 'I', 'R'])].groupby('metric', as_index=False).agg({
                'framework': 'first',
                'dummy': 'first',
                'value': 'mean'
            })

        return average_scores

    def visualize(self, scores: pandas.DataFrame, filename: str, config: dict):
        _config = dict(
            radialaxis=dict(
                range=[0, 2],
                tickvals=[0, 0.5, 1, 1.5, 2],
                ticklen=2,  # Set length of radial ticks
            ),
        )

        conf = copy.deepcopy(_config)
        mergedicts(conf, config)

        self._visualize(scores, filename, conf)
