import copy
import json
import os
import pandas
import requests
from requests.auth import HTTPBasicAuth
from tqdm import tqdm

from frameworks.framework import Framework, AssessmentError, mergedicts


class FUJI(Framework):
    _username = 'marvel'
    _password = 'wonderwoman'

    _base_url = "https://purl.org/fair-sensor-services/"
    _request_url = 'http://localhost:1071/fuji/api/v1/evaluate'
    _request_body = {
        "object_identifier": None,
        "test_debug": True,
        "metadata_service_endpoint": "http://ws.pangaea.de/oai/provider",
        "metadata_service_type": "oai_pmh",
        "use_datacite": True,
        "use_github": False,
        "metric_version": "metrics_v0.5"
    }

    def __init__(self, prefix):
        Framework.__init__(self, f'{self._base_url}{prefix}/')
        self._all_scores = pandas.DataFrame(
            columns=["framework", "dummy", "metric", "value"])

    @property
    def name(self) -> str:
        return 'f-uji'

    def assess(self, identifier: str):
        self._request_body["object_identifier"] = f"{self._prefix_url}{identifier}/"
        response = requests.post(self._request_url, json=self._request_body,
                                 auth=HTTPBasicAuth(self._username, self._password))

        if response.status_code == 200:
            json.dump(json.loads(response.content.decode('UTF-8')),
                      open(os.path.join(self._output_dir, f'{identifier}.json'), 'w'))
        else:
            raise AssessmentError(response.reason)

    def evaluate(self, assessment_folder: str, dummy: str, mode: str) -> pandas.DataFrame:
        directory = os.path.join(assessment_folder, self.name, dummy, mode)

        for assessment_filename in tqdm(os.listdir(directory)):
            with open(os.path.join(directory, assessment_filename)) as assessment_file:
                assessment = json.load(assessment_file)

            for key in assessment['summary']['score_percent']:
                self._all_scores.loc[len(self._all_scores.index)] = [self.name, dummy, key,
                                                                     assessment['summary']['score_percent'][key]]

        average_scores = self._all_scores.groupby('metric', as_index=False).agg({
            'framework': 'first',
            'dummy': 'first',
            'value': 'mean'
        })

        return average_scores

    def convert(self) -> pandas.DataFrame:
        average_scores = \
            self._all_scores[self._all_scores['metric'].isin(['F', 'A', 'I', 'R'])].groupby('metric',
                                                                                            as_index=False).agg({
                'framework': 'first',
                'dummy': 'first',
                'value': 'mean'
            })

        return average_scores

    def visualize(self, scores: pandas.DataFrame, filename: str, config: dict = None):
        _config = dict(
            radialaxis=dict(
                range=[0, 100]
            ),
        )

        conf = copy.deepcopy(_config)
        mergedicts(conf, config)

        conf = {**_config, **config} if config is not None else _config
        self._visualize(scores, filename, conf)
