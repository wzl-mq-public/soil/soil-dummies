import os

import copy

import abc
import pandas
import plotly
import plotly.express

colors = ['#00549f', '#006165', '#0098A1', '#57AB27', '#BDCD00', '#CC071E', '#A11035', '#612158', '#7A6FAC']


def mergedicts(a, b):
    if b is None:
        return

    for key in b:
        if isinstance(a.get(key), dict) or isinstance(b.get(key), dict):
            mergedicts(a[key], b[key])
        else:
            a[key] = b[key]


class AssessmentError(Exception):

    def __init__(self, reason: str):
        self._reason = reason

    def __str__(self):
        return f'AssessmentError: {self._reason}'


class Framework(abc.ABC):

    def __init__(self, prefix_url: str):
        self._prefix_url = prefix_url
        self._output_dir = None

    @property
    @abc.abstractmethod
    def name(self) -> str:
        ...

    def init_assessment(self, base_path: str, dummy: str, mode: str):
        if not os.path.exists(os.path.join(base_path, self.name)):
            os.mkdir(os.path.join(base_path, self.name))

        if not os.path.exists(os.path.join(base_path, self.name, dummy)):
            os.mkdir(os.path.join(base_path, self.name, dummy))

        if not os.path.exists(os.path.join(base_path, self.name, dummy, mode)):
            os.mkdir(os.path.join(base_path, self.name, dummy, mode))

        self._output_dir = os.path.join(base_path, self.name, dummy, mode)

    @abc.abstractmethod
    def assess(self, identifier: str):
        ...

    @abc.abstractmethod
    def evaluate(self, assessment_folder: str, dummy: str, mode: str) -> pandas.DataFrame:
        ...

    @abc.abstractmethod
    def convert(self) -> pandas.DataFrame:
        ...

    @abc.abstractmethod
    def visualize(self, scores: pandas.DataFrame, filename: str, config: dict):
        ...

    def _visualize(self, scores: pandas.DataFrame, filename: str, config: dict):
        if 'framework' in scores.columns:
            fig = plotly.express.line_polar(scores, r='value', theta='metric', line_close=True, color='framework',
                                            labels='framework', color_discrete_sequence=colors)
        else:
            fig = plotly.express.line_polar(scores, r='value', theta='metric', line_close=True)

        fig.update_traces(fill='toself', textfont=(dict(color='black')))

        width_mm = 86
        height_mm = 86

        conversion_factor = 3.78  # rough approximation factor for DPI=96
        width_px = width_mm * conversion_factor
        height_px = height_mm * conversion_factor

        # Customize the layout
        layout_config = dict(
            bgcolor='rgba(0,0,0,0)',  # Set radar plot background color (transparent in this case)
            radialaxis=dict(
                visible=True,
                range=[0, 100],  # Set the range of the radial axis
                tickcolor='black',  # Set the color of the tick marks
                tickvals=[0, 25, 50, 75, 100],  # Set the manual step size
                tickfont=dict(
                    size=10,  # Set the font size of the tick labels
                    color='black',
                ),
                showline=False,  # Show radial axis line
                linecolor='black',
                gridcolor='black',  # Set color of radial grid lines
                griddash='dash',
            ),
            angularaxis=dict(
                showline=True,  # Show angular axis line
                linecolor='black',  # Set color of angular axis line
                tickcolor='black',
                tickfont=dict(
                    size=10,  # Set the font size of the tick labels
                    color='black',
                ),
            ),
        )

        conf = copy.deepcopy(layout_config)

        mergedicts(conf, config)

        fig.update_layout(polar=conf,
                          paper_bgcolor='white',  # Set background color outside the radar plot
                          showlegend=False,
                          height=height_px,
                          width=width_px,
                          margin=dict(t=20, b=20, l=20, r=20), )

        fig.write_image(filename)
        # fig.show()
