import json
import logging
import os
import pyshacl
import rdflib
import requests
import toml
from wzl.soil.semantics import Namespaces

import log

SRC = os.path.join('..', 'src')
OUT = os.path.join('..', 'out', 'validation')

RELOAD_SHAPES = True
RELOAD_DATA = True
RELOAD_VOCABS = True

LOAD_SOIL_SHAPES_ONLY = True

loaded_shapes = []
loaded_metadata = []

logger = log.get('validation', OUT)


def load_vocabs():
    vocab_graph = rdflib.Graph()

    with open('vocabs.txt', 'r') as vocabs_file:
        for line in vocabs_file:
            if line[0] != '#':
                line = line.replace('\n', '').replace('\r', '')
                response = requests.get(line)

                if response.status_code == 200:
                    vocab_graph += rdflib.Graph().parse(response.content, format='n3')
                    logger.debug(f'Collecting {line} ...success.')
                else:
                    logger.error(f'Collecting {line} ...failure. Reason: {response.reason}')

    return vocab_graph


def collect_shapes(request_url):
    global loaded_shapes
    shapes_graph = rdflib.Graph()
    temp_graph = rdflib.Graph()

    if request_url in loaded_shapes:
        return shapes_graph

    response = requests.get(f'{request_url}/?format=turtle')

    if response.status_code == 200:
        temp_graph.parse(response.content, 'n3')

        if not LOAD_SOIL_SHAPES_ONLY or 'purl.org/soil' in request_url:
            logger.debug(f'Collecting {request_url} ...success and added the profile to graph.')
            shapes_graph += temp_graph
        else:
            logger.debug(f'Collecting {request_url} ...success.')

        loaded_shapes += [request_url]

        imports = temp_graph.objects(subject=rdflib.URIRef(request_url), predicate=rdflib.OWL.imports)
        for import_ in imports:
            shapes_graph += collect_shapes(import_.toPython())

    else:
        logger.error(f'Collecting {request_url} ...failure. Reason: {response.reason}')

    return shapes_graph


def collect_children(parent_metadata, subject, predicate):
    metadata = rdflib.Graph()

    children = parent_metadata.objects(subject=subject, predicate=predicate)
    # children = parent_metadata.triples((None, predicate, None))

    for child in children:
        # print(child)
        metadata += collect_metadata(child.toPython())

    return metadata


def collect_metadata(request_url):
    global loaded_metadata

    metadata = rdflib.Graph()

    if request_url in loaded_metadata:
        return metadata

    response = requests.get(f'{request_url}?format=turtle')

    if response.status_code == 200:
        metadata.parse(response.content, format='n3')
        logger.debug(f'Collecting {request_url} ...success.')

        loaded_metadata += [request_url]

        metadata += collect_children(metadata, rdflib.URIRef(request_url), Namespaces.ssn.implements)  # Functions
        metadata += collect_children(metadata, rdflib.URIRef(request_url), Namespaces.ssn.hasSubSystem)  # Components
        metadata += collect_children(metadata, rdflib.URIRef(request_url), Namespaces.ssn.hasProperty)  # Parameters
        metadata += collect_children(metadata, rdflib.URIRef(request_url),
                                     Namespaces.sosa.observes)  # Measurements metadata
        metadata += collect_children(metadata, rdflib.URIRef(request_url), Namespaces.ssn.hasInput)  # Arguments
        metadata += collect_children(metadata, rdflib.URIRef(request_url), Namespaces.ssn.hasOutput)  # Returns
        metadata += collect_children(metadata, rdflib.URIRef(request_url),
                                     Namespaces.sosa.madeObservation)  # Measurements data
        metadata += collect_children(metadata, rdflib.URIRef(request_url),
                                     Namespaces.sosa.hasResult)  # Measurements Result
        metadata += collect_children(metadata, rdflib.URIRef(request_url),
                                     Namespaces.m4i.hasUncertaintyDeclaration)  # Uncertainty

    else:
        logger.error(f'Collecting {request_url} ...failure. Reason: {response.reason}')

    return metadata


if __name__ == '__main__':
    for dummy in ['lasertracker', 'monitoring', 'robot']:

        logger.info(f'Testing Dummy "{dummy}"...')

        if not os.path.exists(os.path.join(OUT, dummy)):
            os.mkdir(os.path.join(OUT, dummy))

        config = toml.load(os.path.join(SRC, dummy, 'config.toml'))
        prefix_url = config["semantic"]["url"]

        model_path = f'{SRC}/{dummy}/'
        model = json.load(open(os.path.join(model_path, 'model.json')))
        identifier = model['uuid'][4:].capitalize()

        if RELOAD_SHAPES:
            shapes_graph = collect_shapes(f'{prefix_url}{model["profile"]}Profile')
            shapes_graph.serialize(destination=os.path.join(OUT, dummy, 'shapes.ttl'))
        else:
            shapes_graph = rdflib.Graph().parse(source=os.path.join(OUT, dummy, 'shapes.ttl'), format='n3')

        if RELOAD_DATA:
            data_graph = collect_metadata(f'{prefix_url}{identifier}')
            data_graph.serialize(destination=os.path.join(OUT, dummy, 'metadata.ttl'))
        else:
            data_graph = rdflib.Graph().parse(source=os.path.join(OUT, dummy, 'metadata.ttl'))

        if RELOAD_VOCABS:
            vocabs_graph = load_vocabs()
            vocabs_graph.serialize(destination=os.path.join(OUT, 'vocabs.ttl'))
        else:
            vocabs_graph = rdflib.Graph().parse(source=os.path.join(OUT, 'vocabs.ttl'), format='n3')

        conforms, results_graph, results_text = pyshacl.validate(data_graph + vocabs_graph,
                                                                 shacl_graph=shapes_graph,
                                                                 advanced=True)
        if conforms:
            logger.info(f'Validation result for dummy "{dummy}":\n{results_text}')
        else:
            logger.error(f'Validation result for dummy "{dummy}":\n{results_text}')

        results_graph.serialize(destination=os.path.join(OUT, dummy, 'results.ttl'))
