import json
import os
import toml
from tqdm import tqdm

import log
from frameworks.framework import Framework, AssessmentError
from frameworks.fairchecker import FAIRChecker
from frameworks.fuji import FUJI
from frameworks.fairenough import FAIREnough

SRC = os.path.join('..', 'src')
OUT = os.path.join('..', 'out')

PROFILES = True
METADATA = True

logger = log.get('assessment', OUT)


def assess_metadata(framework: Framework, dummy: str, element: dict, parent_id: str = "", recursive: bool = True):
    identifier = parent_id + element['uuid'][4:].capitalize() if recursive else parent_id

    try:
        framework.assess(identifier)
        logger.debug(f'Assessing {identifier}...success.')
    except AssessmentError as e:
        logger.error(f'Assessing {identifier}...failure. Reason: {e}')

    if element['uuid'][:3] == 'COM':
        for child in element['measurements'] + element['parameters'] + element['functions'] + element[
            'components']:
            assess_metadata(framework, dummy, child, identifier)

    if element['uuid'][:3] == 'MEA' and recursive:
        assess_metadata(framework, dummy, element, f'{identifier}Measurement', recursive=False)
        assess_metadata(framework, dummy, element, f'{identifier}MeasurementResult', recursive=False)
        assess_metadata(framework, dummy, element, f'{identifier}MeasurementUncertainty', recursive=False)

    if element['uuid'][:3] in ['MEA', 'RET', 'ARG', 'PAR'] and recursive:
        if element['datatype'] in ['float', 'int', 'enum', 'time']:
            assess_metadata(framework, dummy, element, f'{identifier}Range', recursive=False)

    if element['uuid'][:3] == 'FUN':
        for child in element['arguments'] + element['returns']:
            assess_metadata(framework, dummy, child, identifier)


def assess_profiles(framework, dummy):
    profiles_path = f'{SRC}/{dummy}/{config["semantic"]["profiles"]}'

    for filename in tqdm(os.listdir(profiles_path)):
        profile_name = f'{filename[:-10]}Profile'

        try:
            framework.assess(profile_name)
            logger.debug(f'Assessing {profile_name}...success.')
        except AssessmentError as e:
            logger.error(f'Assessing {profile_name}...failure. Reason: {e}')


if __name__ == '__main__':
    for dummy in ['lasertracker', 'monitoring', 'robot']:
        config = toml.load(os.path.join(SRC, dummy, 'config.toml'))

        # frameworks = [FUJI(config['semantic']['prefix']), FAIRChecker(config['semantic']['prefix'])]
        frameworks = [FAIREnough(config['semantic']['prefix'],'fair-evaluator-maturity-indicators'),
                      FAIREnough(config['semantic']['prefix'],'fair-enough-metadata'),
                      FAIREnough(config['semantic']['prefix'],'fair-enough-data')]

        for framework in frameworks:

            logger.info(f'Assessing dummy "{dummy}" using framework "{framework.name}"...')

            if PROFILES:
                framework.init_assessment(OUT, dummy, 'profiles')
                assess_profiles(framework, dummy)

            if METADATA:
                framework.init_assessment(OUT, dummy, 'metadata')

                model_path = f'{SRC}/{dummy}/'
                model = json.load(open(os.path.join(model_path, 'model.json')))

                assess_metadata(framework, dummy, model)

            logger.info(f'Finished assessing dummy "{dummy}" using framework "{framework.name}".')
