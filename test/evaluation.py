import os
import pandas
import toml

import log
from frameworks.fairchecker import FAIRChecker
from frameworks.fairenough import FAIREnough
from frameworks.fuji import FUJI

SRC = os.path.join('..', 'src')
ASSESSMENTS = os.path.join('..', 'out')
OUT = os.path.join('..', 'out')

PROFILES = False
METADATA = True

logger = log.get('evaluation', OUT)


def evaluate(mode: str):
    dummies = ['lasertracker', 'monitoring', 'robot']
    all_scores = pandas.DataFrame(columns=["framework", "dummy", "metric", "value"])
    fair_scores = pandas.DataFrame(columns=["framework", "dummy", "metric", "value"])

    for dummy in dummies:

        config = toml.load(os.path.join(SRC, dummy, 'config.toml'))

        frameworks = [FUJI(config['semantic']['prefix']),
                      # FAIREnough(config['semantic']['prefix'], 'fair-evaluator-maturity-indicators'),
                      FAIREnough(config['semantic']['prefix'], 'fair-enough-metadata'),
                      FAIREnough(config['semantic']['prefix'], 'fair-enough-data'),
                      FAIRChecker(config['semantic']['prefix']),
                      ]

        for framework in frameworks:
            score = framework.evaluate(ASSESSMENTS, dummy, mode)
            logger.info(score)
            all_scores = pandas.concat([all_scores, score], ignore_index=True)
            fair_scores = pandas.concat([fair_scores, framework.convert()], ignore_index=True)

    for dummy in dummies:
        config = toml.load(os.path.join(SRC, dummy, 'config.toml'))
        frameworks = [FUJI(config['semantic']['prefix']), FAIRChecker(config['semantic']['prefix'])]
        for framework in frameworks:
            average_all_scores = all_scores[all_scores['framework'] == framework.name].groupby(['metric'],
                                                                                               as_index=False).agg(
                {'value': 'mean'})
            framework.visualize(average_all_scores, os.path.join(OUT, f'{mode}_{framework.name}.svg'),
                                dict(radialaxis=dict(showline=True)))

    average_scores = fair_scores.groupby(['framework', 'metric'], as_index=False).agg({'value': 'mean'})
    average_scores['metric'] = average_scores['metric'].replace('F', 'Findability')
    average_scores['metric'] = average_scores['metric'].replace('A', 'Accessibility')
    average_scores['metric'] = average_scores['metric'].replace('I', 'Interoperability')
    average_scores['metric'] = average_scores['metric'].replace('R', 'Reusability')

    logger.info(average_scores)

    # average_profile_scores = [profile_scores[key].mean() for key in profile_scores]
    # average_metadata_scores = [metadata_scores[key].mean() for key in metadata_scores]

    FUJI(config['semantic']['prefix']).visualize(average_scores, os.path.join(OUT, f'{mode}_fair.svg'))


if __name__ == '__main__':
    evaluate('profiles')
    evaluate('metadata')
