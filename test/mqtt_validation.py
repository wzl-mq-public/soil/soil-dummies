import json
import os
import pyshacl
import rdflib
import requests
import toml
import time
from wzl import mqtt
from wzl.soil.semantics import Namespaces

import log

config = toml.load('config.toml')

SRC = os.path.join('..', 'src')
OUT = os.path.join('..', 'out', 'validation')

RELOAD_SHAPES = False
RELOAD_DATA = True
RELOAD_VOCABS = False

LOAD_SOIL_SHAPES_ONLY = True

loaded_shapes = []
loaded_metadata = []

data_graph = rdflib.Graph()

logger = log.get('validation', OUT)

MQTT_USER = config['MQTT_USER']
MQTT_PASSWORD = config['MQTT_PASSWORD']

MQTT_BROKER = config['MQTT_BROKER']
MQTT_VHOST = config['MQTT_VHOST']

def load_vocabs():
    vocab_graph = rdflib.Graph()

    with open('vocabs.txt', 'r') as vocabs_file:
        for line in vocabs_file:
            if line[0] != '#':
                line = line.replace('\n', '').replace('\r', '')
                response = requests.get(line)

                if response.status_code == 200:
                    vocab_graph += rdflib.Graph().parse(response.content, format='n3')
                    logger.debug(f'Collecting {line} ...success.')
                else:
                    logger.error(f'Collecting {line} ...failure. Reason: {response.reason}')

    return vocab_graph


def collect_shapes(request_url):
    global loaded_shapes
    shapes_graph = rdflib.Graph()
    temp_graph = rdflib.Graph()

    if request_url in loaded_shapes:
        return shapes_graph

    response = requests.get(f'{request_url}/?format=turtle')

    if response.status_code == 200:
        temp_graph.parse(response.content, 'n3')

        if not LOAD_SOIL_SHAPES_ONLY or 'purl.org/soil' in request_url:
            logger.debug(f'Collecting {request_url} ...success and added the profile to graph.')
            shapes_graph += temp_graph
        else:
            logger.debug(f'Collecting {request_url} ...success.')

        loaded_shapes += [request_url]

        imports = temp_graph.objects(subject=rdflib.URIRef(request_url), predicate=rdflib.OWL.imports)
        for import_ in imports:
            shapes_graph += collect_shapes(import_.toPython())

    else:
        logger.error(f'Collecting {request_url} ...failure. Reason: {response.reason}')

    return shapes_graph

def store(topic, message):
    global data_graph

    data = rdflib.Graph()
    data.parse(message.decode('UTF-8'), 'n3')
    data_graph += data

def collect_metadata(topic, client):
    client.subscribe(topic, 0)
    time.sleep(10)
    client.unsubscribe(topic)

if __name__ == '__main__':
    for dummy in ['lasertracker', 'monitoring', 'robot']:

        logger.info(f'Testing Dummy "{dummy}"...')

        if not os.path.exists(os.path.join(OUT, dummy)):
            os.mkdir(os.path.join(OUT, dummy))

        config = toml.load(os.path.join(SRC, dummy, 'config.toml'))
        prefix_url = config["semantic"]["url"]

        model_path = f'{SRC}/{dummy}/'
        model = json.load(open(os.path.join(model_path, 'model.json')))
        identifier = model['uuid'][4:].capitalize()

        if RELOAD_SHAPES:
            shapes_graph = collect_shapes(f'{prefix_url}{model["profile"]}Profile')
            shapes_graph.serialize(destination=os.path.join(OUT, dummy, 'shapes.ttl'))
        else:
            shapes_graph = rdflib.Graph().parse(source=os.path.join(OUT, dummy, 'shapes.ttl'), format='n3')

        if RELOAD_DATA:
            client = mqtt.MQTTSubscriber()
            client.connect(MQTT_BROKER, MQTT_USER, MQTT_PASSWORD, vhost=MQTT_VHOST, ssl=True)

            client.set_callback("PRINT", store)

            data_graph = rdflib.Graph()

            collect_metadata(f'{prefix_url}#'[8:], client)
            data_graph.serialize(destination=os.path.join(OUT, dummy, 'mqtt_metadata.ttl'))
        else:
            data_graph = rdflib.Graph().parse(source=os.path.join(OUT, dummy, 'mqtt_metadata.ttl'))

        if RELOAD_VOCABS:
            vocabs_graph = load_vocabs()
            vocabs_graph.serialize(destination=os.path.join(OUT, 'vocabs.ttl'))
        else:
            vocabs_graph = rdflib.Graph().parse(source=os.path.join(OUT, 'vocabs.ttl'), format='n3')

        conforms, results_graph, results_text = pyshacl.validate(data_graph + vocabs_graph,
                                                                 shacl_graph=shapes_graph,
                                                                 advanced=True)
        if conforms:
            logger.info(f'Validation result for dummy "{dummy}":\n{results_text}')
        else:
            logger.error(f'Validation result for dummy "{dummy}":\n{results_text}')

        results_graph.serialize(destination=os.path.join(OUT, dummy, 'results.ttl'))
