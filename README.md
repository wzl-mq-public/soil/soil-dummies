# SOIL Dummies

The repository contains the implementation of demonstrators of the _SensOr Interfacing Language (SOIL)_.  
An explanation of SOIL can found [here](https://doi.org/10.5281/zenodo.7757249).  
**Developer in charge: [Matthias Bodenbenner](mailto:m.bodenbenner@wzl-mq.rwth--achen.de)**

## Setup

To execute the dummy-servers on your local machine follow these instructions. You must have installed Python (at least
Python 3.6) and you need a (local) MQTT Broker to send the data via MQTT as well as appropriate credentials for the
servers.

1. Clone the repository

```cmd
git clone https://git-ce.rwth-aachen.de/wzl-mq-public/soil/soil-dummies
```

2. Install the requirements

```cmd
pip install requirements.txt
```

3. Generate the source code template
```cmd
init.bat
```

4. Copy the file _config-sample.toml_ into the folders of the dummies and rename it to _config.toml_. Adjust the config,
   if required.

5. Execute the server(s) via the following command in the respective source folders of the dummy servers.

```cmd
python main.py
```

## Documentation

Work in progress... unfortunately.

## Citation & References

[Bodenbenner, Matthias](mailto:m.bodenbenner@wzl-mq.rwth-aachen.de). (2023). Description and Manual of the SensOr Interfacing Language (1.0.1). Zenodo. https://doi.org/10.5281/zenodo.7778563 

## Acknowledgements
Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under Germany's Excellence Strategy – EXC-2023 Internet of Production – 390621612.

Funded by the Deutsche Forschungsgemeinschaft (DFG, German Research Foundation) under Project-ID 432233186 -- AIMS.

## License of used third-party libraries

| Library  | License                     |
|----------|-----------------------------|
| numpy    | BSD License                 |
| requests | BSD License                 |
| toml     | MIT License                 |
| wzl-udi  | MIT License                 |